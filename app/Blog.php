<?php
/**
 * Created by PhpStorm.
 * User: YJC
 * Date: 2017/6/7 007
 * Time: 22:23
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    public function listAll($offset = 0, $limit = 10){
        return $this->select('title', 'content')->where('flag', '=', '1')->orderBy('id', 'desc')->offset($offset)->limit($limit)->get();
//        return $this->where('flag', '=', '1')->paginate($limit);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}