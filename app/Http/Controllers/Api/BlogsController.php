<?php
/**
 * Created by PhpStorm.
 * User: YJC
 * Date: 2017/6/7 007
 * Time: 22:16
 */

namespace App\Http\Controllers\Api;

use App\Blog;
use Illuminate\Http\Request;

class BlogsController extends Controller
{

    /**
     * 文章列表
     */
    public function index(Request $request){
        $offset = $request->offset ? : 0;
        $limit = $request->limit ? : 10;

        $blog_model = new Blog();
        $blogs = $blog_model->listAll($offset, $limit);

        return returnSucc($blogs);
    }

    public function create(){}

    public function show($id){

        $blog = Blog::findOrFail($id);

        return returnSucc($blog);
    }

    public function author($id){
        $blog = Blog::findOrFail($id);
        return returnSucc($blog->user);
    }

}