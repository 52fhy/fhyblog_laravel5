<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

//根据命名空间分组
Route::group(['namespace' => 'Api'], function()
{
    //Route::get('blogs/{offset?}/{limit?}', 'BlogsController@index');
    Route::get('blogs', 'BlogsController@index');
    Route::get('blogs/{id}', 'BlogsController@show');
    Route::get('blogs/{id}/author', 'BlogsController@author');
});
