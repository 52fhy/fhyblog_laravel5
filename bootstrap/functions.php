<?php

/**
 * Created by PhpStorm.
 * User: YJC
 * Date: 2017/6/7 007
 * Time: 23:37
 */
function returnSucc($status = 0, $msg = '成功', $data = []){
    if(!is_int($status)){
        $data = $status;
        $status = 0;
    }
    return response()->json(['status'=>$status,'msg'=>$msg,'result'=>$data]);
}

function returnError($status = 9999, $msg = '失败', $data = []){
    return response()->json(['status'=>$status,'msg'=>$msg,'result'=>$data]);
}